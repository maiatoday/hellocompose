package net.maiatoday.hellocompose

import android.content.res.Configuration
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.Crossfade
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.absolutePadding
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Card
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import net.maiatoday.hellocompose.data.sampleStrings
import net.maiatoday.hellocompose.data.sampleWords
import net.maiatoday.hellocompose.ui.ConfettiShape
import net.maiatoday.hellocompose.ui.Rabbit
import net.maiatoday.hellocompose.ui.RainbowText
import net.maiatoday.hellocompose.ui.SampleCard2
import net.maiatoday.hellocompose.ui.SwitchCard
import net.maiatoday.hellocompose.ui.confetti
import net.maiatoday.hellocompose.ui.theme.HelloComposeTheme
import net.maiatoday.hellocompose.ui.theme.SkittlesRainbow

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            HelloComposeTheme {
                MainScreen()
            }
        }
    }
}

@Preview(
    showBackground = true,
    uiMode = Configuration.UI_MODE_NIGHT_NO or Configuration.UI_MODE_TYPE_NORMAL,
    backgroundColor = 0xFFFFFFFF, name = "Day preview"
)
@Composable
fun DefaultPreview() {
    HelloComposeTheme {
        MainScreen()
    }
}


@OptIn(ExperimentalAnimationApi::class)
@Composable
fun MainScreen() {
    // A surface container using the 'background' color from the theme
    Surface(modifier = Modifier.fillMaxSize(), color = MaterialTheme.colorScheme.background) {
        var text by remember { mutableStateOf("") }
        var show by remember { mutableStateOf(false) }
        Box(
            modifier = Modifier
                .fillMaxSize()
                .confetti(
                    contentColors = SkittlesRainbow,
                    confettiShape = ConfettiShape.Mixed,
                    speed = 0.1F,
                    populationFactor = 0.3f,
                    isVisible = show
                )
        ) {
            Column(horizontalAlignment = Alignment.CenterHorizontally) {
                OutlinedTextField(
                    modifier = Modifier
                        .fillMaxWidth(0.5f)
                        .padding(16.dp),
                    value = text,
                    singleLine = true,
                    onValueChange = { text = it },
                    label = {
                        androidx.compose.material.Text(
                            "Rainbow text",
                            color = androidx.compose.material.MaterialTheme.colors.secondary
                        )
                    },
                    colors = TextFieldDefaults.outlinedTextFieldColors(focusedBorderColor = androidx.compose.material.MaterialTheme.colors.secondary),
                )
                RainbowText(
                    modifier = Modifier.padding(16.dp),
                    text = text,
                    style = androidx.compose.material.MaterialTheme.typography.h3
                )
                Row {
                    // SwitchCard(false)
                    Column(horizontalAlignment = Alignment.CenterHorizontally) {
                        val buttonColor: Color by animateColorAsState(
                            targetValue = if (show) {
                                androidx.compose.material.MaterialTheme.colors.secondary
                            } else {
                                androidx.compose.material.MaterialTheme.colors.primary
                            },
                            animationSpec = tween(durationMillis = 3000)
                        )

                        val contentColor: Color by animateColorAsState(
                            targetValue = if (show) {
                                androidx.compose.material.MaterialTheme.colors.onSecondary
                            } else {
                                androidx.compose.material.MaterialTheme.colors.onPrimary
                            },
                            animationSpec = tween(durationMillis = 3000)
                        )

                        Button(
                            modifier = Modifier
                                .padding(16.dp)
                                .width(120.dp), onClick = {
                                show = !show
                            }, colors = ButtonDefaults.buttonColors(
                                backgroundColor = buttonColor,
                                contentColor = contentColor

                            )
                        ) {
                            androidx.compose.material.Text(
                                text = if (show) "Hide" else "Show",
                                modifier = Modifier.padding(8.dp)
                            )

                        }
                        AnimatedVisibility(show) {
                            Rabbit()
                        }
                    }
                    SwitchCard(true)
                }

                SampleCard2(
                    "European Rabbit",
                    "a short tale",
                    """The European Rabbit (Oryctolagus cuniculus) has been officially reclassified as “Near Threatened” with extinction, in its native range, by the International Union for Conservation of Nature (IUCN). Many people remain unaware that the European Rabbit is native to just Spain, Portugal and small parts of North Africa, from where it was introduced elsewhere by humans (e.g. into the UK and Australia). Similarly, many do not know that rabbits have declined massively in the Iberian Peninsula in recent decades, and that this has had a huge impact on wider nature conservation given that rabbits are a vital prey species for many other animals. """.trimIndent()
                )

            }
        }
    }
}
