package net.maiatoday.hellocompose.data

fun sampleStrings(count: Int = 100) = sampleWords.take(count)

val sampleWords =
    listOf(
        "lamentable",
        "tightfisted",
        "marked",
        "striped",
        "bawdy",
        "stamp",
        "faint",
        "abnormal",
        "watery",
        "examine",
        "condemned",
        "stroke",
        "sidewalk",
        "voyage",
        "null",
        "juicy",
        "decorate",
        "purring",
        "tray",
        "utter",
        "invention",
        "face",
        "market",
        "quiver",
        "rob",
        "polite",
        "pricey",
        "shy",
        "lying",
        "time",
        "wakeful",
        "top",
        "devilish",
        "demonic",
        "tire",
        "keen",
        "wound",
        "ship",
        "decisive",
        "hanging",
        "practice",
        "confuse",
        "instrument",
        "tail",
        "nail",
        "pump",
        "voice",
        "thunder",
        "dogs",
        "day",
        "wax",
        "clap",
        "irritating",
        "pop",
        "punch",
        "authority",
        "chivalrous",
        "legal",
        "suggest",
        "throne",
        "languid",
        "power",
        "divergent",
        "cap",
        "agree",
        "excite",
        "orange",
        "night",
        "company",
        "brick",
        "current",
        "sparkling",
        "tow",
        "piquant",
        "dirty",
        "organic",
        "tame",
        "muscle",
        "desk",
        "popcorn",
        "obtain",
        "ancient",
        "one",
        "attack",
        "gratis",
        "river",
        "earsplitting",
        "flower",
        "sound",
        "majestic",
        "drip",
        "responsible",
        "ill",
        "suspend",
        "vacuous",
        "four",
        "whirl",
        "thin",
        "low",
        "run",
    )
