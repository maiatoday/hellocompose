package net.maiatoday.hellocompose.ui

import androidx.compose.animation.Crossfade
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Card
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import net.maiatoday.hellocompose.R
import net.maiatoday.hellocompose.data.sampleStrings

//SampleCard2(
//"European Rabbit",
//"a short tale",
//"The European Rabbit (Oryctolagus cuniculus) has been officially reclassified as “Near Threatened” with\n" +
//"extinction, in its native range, by the International Union for Conservation of Nature (IUCN). Many people\n" +
//"remain unaware that the European Rabbit is native to just Spain, Portugal and small parts of North Africa,\n" +
//"from where it was introduced elsewhere by humans (e.g. into the UK and Australia). Similarly, many do not\n" +
//"know that rabbits have declined massively in the Iberian Peninsula in recent decades, and that this has had a\n" +
//"huge impact on wider nature conservation given that rabbits are a vital prey species for many other animals. "
//)
@OptIn(ExperimentalMaterialApi::class)
@Composable
fun SampleCard2(title: String, shortDescription: String, longDescription: String) {
    var expanded by remember { mutableStateOf(false) }
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp),
        elevation = 10.dp,
        onClick = { expanded = !expanded }
    ) {
        Row {
            Rabbit()
            Column {
                Text(
                    text = title,
                    style = MaterialTheme.typography.h5,
                    modifier = Modifier
                        .padding(16.dp)
                )
                Text(
                    text = if (expanded) longDescription else shortDescription,
                    style = MaterialTheme.typography.body2,
                    modifier = Modifier
                        .padding(16.dp)
                        .animateContentSize(animationSpec = tween(3000))
                )
            }
        }
    }
}

@Composable
fun LabelList(label: String) {
    Text(
        text = label,
        modifier = Modifier.padding(8.dp)
    )
}

@Composable
fun SampleList(entries: List<String> = sampleStrings()) {
    Column {
        entries.forEach { item ->
            LabelList(item)
        }
    }
}

@Composable
fun SampleList2(entries: List<String> = sampleStrings()) {
    LazyColumn {
        items(entries.size) { index ->
            LabelList(entries[index])
        }
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun SwitchCard(crossFade: Boolean = false) {
    var suit by remember { mutableStateOf(Suit.HEART) }
    Card(
        modifier = Modifier
            .padding(16.dp),
        elevation = 10.dp,
        onClick = { suit = nextSuit(suit) }
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            if (crossFade) {
                Crossfade(
                    targetState = suit,
                    animationSpec = tween(3000)
                ) { suit ->
                    RedBlackCard(suit)
                }
            } else {
                RedBlackCard(suit)
            }
        }
    }
}

fun nextSuit(suit: Suit): Suit = when (suit) {
    Suit.CLUB -> Suit.DIAMOND
    Suit.DIAMOND -> Suit.SPADE
    Suit.SPADE -> Suit.HEART
    Suit.HEART -> Suit.CLUB
}

@Composable
fun RedBlackCard(suit: Suit) {
    val imageModifier = Modifier.width(150.dp).height(200.dp).padding(32.dp)
    when (suit) {
        Suit.CLUB -> {
            Image(
                modifier = imageModifier,
                painter = painterResource(R.drawable.club),
                contentDescription = "Club",
            )
        }
        Suit.DIAMOND -> {
            Image(
                modifier = imageModifier,
                painter = painterResource(R.drawable.diamond),
                contentDescription = "Diamond",
            )
        }
        Suit.SPADE -> {
            Image(
                modifier = imageModifier,
                painter = painterResource(R.drawable.spade),
                contentDescription = "Spade",
            )
        }
        Suit.HEART -> {
            Image(
                modifier = imageModifier,
                painter = painterResource(R.drawable.heart),
                contentDescription = "Heart",
            )
        }
    }
}

enum class Suit {
    CLUB, DIAMOND, SPADE, HEART
}

@Composable
fun Rabbit(modifier:Modifier = Modifier) {

    Image(
        modifier = modifier.size(120.dp),
        painter = painterResource(R.drawable.rabbit),
        contentDescription = "Rabbit",
    )

}